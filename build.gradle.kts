import java.io.IOException

val jacksonVersion = "2.9.8"
val argparserVersion ="2.0.7"

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.21"
    id("com.github.johnrengelman.shadow") version "5.0.0"
    id("de.fuerstenau.buildconfig") version "1.1.8"
    id("idea")
}

// TODO Find a better versioning scheme that also works both locally and on GitLab
fun getVersionName(): String {
    try {
        // "git describe --tags" would be more idiomatic
        val process = ProcessBuilder("git", "rev-parse", "--short=8", "HEAD").start()
        process.inputStream.reader(Charsets.UTF_8).use {
            return it.readText().trim()
        }
    } catch (e: IOException) {
        // GitLab doesn"t fetch tags, so we fall back to CI environment variables
        return System.getenv("CI_COMMIT_SHORT_SHA")
    }
}

fun getEnvironment(): String {
    return when {
        System.getenv("CI_COMMIT_TAG") != null -> "production"
        System.getenv("CI") != null -> "staging"
        else -> "localhost"
    }
}

group = "com.fleskesvor"
version = getVersionName()

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("net.dv8tion:JDA:3.8.3_462")
    compile("org.slf4j:slf4j-simple:1.7.25")
    compile("io.github.microutils:kotlin-logging:1.6.26")
    compile("com.opencsv:opencsv:4.5")
    compile("org.ahocorasick:ahocorasick:0.4.0")
    compile("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
    compile("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:$jacksonVersion")
    compile("com.xenomachina:kotlin-argparser:$argparserVersion")
    testCompile("junit:junit:4.12")
    testCompile("org.assertj:assertj-core:3.12.2")
    testCompile("org.mockito:mockito-core:2.25.1")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    jar {
        manifest {
            attributes["Main-Class"] = "ApplicationKt"
            attributes["Version"] = getVersionName()
        }
    }
    shadowJar {
        archiveFileName.set("bacon-bot.jar")
        mustRunAfter(clean)
    }
    register("stage") {
        dependsOn(listOf("shadowJar", "clean", "test"))
    }
}

// Additional properties to be set during build time, and available in BuildConfig class
buildConfig {
    buildConfigField("String", "ENVIRONMENT", getEnvironment())
}