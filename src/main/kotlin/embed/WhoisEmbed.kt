package embed

import net.dv8tion.jda.core.entities.User
import java.time.format.DateTimeFormatter

class WhoisEmbed(user: User): SimpleEmbed(
    user = user,
    fields = listOf(
        Field("Mention", user.asMention, true),
        Field("Registered", user.creationTime.format(DateTimeFormatter.ISO_LOCAL_DATE), true)
    )
)
