package embed

import net.dv8tion.jda.core.entities.EmbedType
import net.dv8tion.jda.core.entities.MessageEmbed
import net.dv8tion.jda.core.entities.User
import java.time.OffsetDateTime

open class SimpleEmbed(
    title: String? = null,
    description: String? = null,
    user: User? = null,
    fields: List<Field>
): MessageEmbed(
    null,
    title,
    description,
    EmbedType.RICH,
    OffsetDateTime.now(),
    16143958,
    when {
        user != null -> Thumbnail(user.avatarUrl, null, 50, 50)
        else -> null
    },
    null,
    when {
        user != null -> AuthorInfo("${user.name}#${user.discriminator}", null, user.avatarUrl, null)
        else -> null
    },
    null,
    null,
    null,
    fields
)
