package service

import embed.SimpleEmbed
import extension.getTextChannelByType
import mu.KotlinLogging
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.MessageEmbed

private val logger = KotlinLogging.logger {}

private val tokenRegex = "(\\S+\"[^\"]+\")|(\\S+)".toRegex()
// An ad-hoc database row, persisted as a rich message
data class Row(var key: String, var values: ArrayList<Pair<String, String>>)

fun saveContent(row: Row, guild: Guild) {
    logger.info { "Saving row $row" }
    val embed = SimpleEmbed(description = row.key, fields = row.values.map {
        MessageEmbed.Field(it.first, it.second, true)
    })
    // FIXME: Use db channel instead
    val channel = guild.getTextChannelByType(config.Guild.Channel.LOGS)
    channel?.sendMessage(embed)?.queue {
        logger.info { "Saved row: ${it.jumpUrl}" }
    }
}

fun getContent(key: String, guild: Guild): String? {
    // FIXME: Use db channel instead
    val channel = guild.getTextChannelByType(config.Guild.Channel.LOGS)
    channel?.iterableHistory?.forEach { message ->
        message.embeds.forEach { embed ->
            if (embed.description == key) {
                val row = message.id to Row(embed.description,
                    embed.fields.map { it.name to it.value } as ArrayList<Pair<String, String>>)
                logger.info { "Found row $row" }
                return message.id
            }
        }
    }
    return null
}

fun dropContent(key: String, guild: Guild) {
    // FIXME: Use db channel instead
    val channel = guild.getTextChannelByType(config.Guild.Channel.LOGS)
    val messageId = getContent(key, guild)
    if (messageId != null) {
        channel?.deleteMessageById(messageId)?.queue{
            logger.info { "Deleted message with id $messageId" }
        }
    }
}

/*
 Convenience function for writing to "database" from chat, eg.
 !db save id123 key1:value1 key2:true key3:"another\" value"
 NOTE: key-values must be separated by a single colon (':') and no space
 */
fun parseContent(content: String): Row? {
    val tokens = tokenRegex.findAll(content).map { it.value }.toList()
    logger.info { "Parsed tokens: $tokens" }
    return try {
        tokens.foldIndexed(Row("", ArrayList())) { index, acc, item ->
            if (index == 0) {
                acc.key = item
            } else {
                val (key, value) = item.split(":", limit = 2)
                acc.values.add(key to value.trim('"'))
            }
            acc
        }
    } catch (e: Exception) {
        null
    }
}
