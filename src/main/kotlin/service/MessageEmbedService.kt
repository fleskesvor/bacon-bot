package service

import applicationStart
import com.fleskesvor.BuildConfig
import embed.SimpleEmbed
import embed.WhoisEmbed
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.MessageEmbed.Field
import extension.toHumanReadable
import mu.KotlinLogging
import java.time.Duration
import java.time.Instant

private val logger = KotlinLogging.logger {}

fun postWhoisEmbedMessage(message: String, channel: MessageChannel) {
    val messageEmbed = createWhoisEmbed(message, channel.jda)
    if (messageEmbed != null) {
        channel.sendMessage(messageEmbed).queue {
            logger.info { "Sent message: ${it.jumpUrl}" }
        }
    }
}

fun postPingEmbedMessage(channel: MessageChannel) {
    val uptime = Duration.between(applicationStart, Instant.now())
    val messageEmbed = SimpleEmbed(
        description = "pong!", fields = listOf(
            Field("Version", BuildConfig.VERSION, true),
            Field("Uptime", uptime.toHumanReadable(), true),
            Field("Environment", BuildConfig.ENVIRONMENT, true)
        )
    )
    channel.sendMessage(messageEmbed).queue {
        logger.info { "Sent message: ${it.jumpUrl}" }
    }
}

private fun createWhoisEmbed(message: String, jda: JDA): WhoisEmbed? {
    if (message.isNotBlank()) {
        val user = jda.getUserById(message.trim().toLong())
        return WhoisEmbed(user)
    }
    return null
}
