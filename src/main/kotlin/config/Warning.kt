package config

import com.fasterxml.jackson.core.type.TypeReference
import config.dto.WarningDto
import config.serialize.BaseConfig

object Warning: BaseConfig<WarningDto>("warnings.yml", object: TypeReference<WarningDto>() {}) {
    val naughtyTextWarning: String = config.`naughty text warning`
    val naughtyEmoteWarning: String = config.`naughty emote warning`
    val embedSpamWarning: String = config.`embed spam warning`
    val kickMemberReason: String = config.`kick member reason`
    val kickMemberMessage: String = config.`kick member message`
}
