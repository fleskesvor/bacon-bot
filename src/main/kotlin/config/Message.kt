package config

import com.fasterxml.jackson.core.type.TypeReference
import config.dto.MessageDto
import config.serialize.BaseConfig

object Message: BaseConfig<Map<Long, MessageDto>>("messages.yml", object: TypeReference<Map<Long, MessageDto>>() {}) {

    fun welcomeMessages(guild: Long) = config[guild]?.`welcome messages`

    fun welcomeMessage(guild: Long, mention: String) = welcomeMessages(guild)?.random()?.format(mention)
}
