package config

import com.fasterxml.jackson.core.type.TypeReference
import config.dto.GuildDto
import config.serialize.BaseConfig
import config.serialize.YamlKey

object Guild: BaseConfig<Map<Long, GuildDto>>("guilds.yml", object: TypeReference<Map<Long, GuildDto>>() {}) {

    const val DFAC = 357444624178216961
    const val BACON_LOVERS = 560575039519326229

    enum class Channel: YamlKey {
        WELCOME,
        LOGS
    }

    fun getChannel(guild: Long, channel: Guild.Channel) = config[guild]?.channels?.get(channel)

    fun getAdminRoles(guild: Long) = config[guild]?.`admin roles`
}
