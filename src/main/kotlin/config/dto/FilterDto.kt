package config.dto

data class FilterDto(
    val `nickname replacement characters`: Map<Char, Char>
)