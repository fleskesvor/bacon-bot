package config.dto

data class MessageDto(
    val `welcome messages`: List<String>
)
