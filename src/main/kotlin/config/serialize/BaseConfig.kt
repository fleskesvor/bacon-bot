package config.serialize

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule

open class BaseConfig<T>(configFile: String, typeReference: TypeReference<T>) {

    val config: T

    init {
        val mapper = ObjectMapper(YAMLFactory())
        mapper.registerModule(KotlinModule())

        val inputStream = this.javaClass.classLoader
            .getResourceAsStream(configFile)
        config = mapper.readValue(inputStream, typeReference)
    }
}
