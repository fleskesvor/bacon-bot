package config.serialize

import com.fasterxml.jackson.annotation.JsonValue

interface YamlKey {
    val name: String

    @JsonValue
    fun getKey(): String {
        return name.toLowerCase()
    }
}