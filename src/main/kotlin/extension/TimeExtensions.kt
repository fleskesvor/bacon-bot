package extension

import java.time.Duration

fun Duration.toHumanReadable(): String {
    return when {
        this.toDays() > 0 -> this.toDaysAndHours()
        this.toHours() > 0 -> this.toHoursAndMinutes()
        this.toMinutes() > 0 -> this.toMinutesAndSeconds()
        else -> this.toSecondsAndMillis()
    }
}

fun Duration.toDaysAndHours() =
    String.format("%sd %sh", this.toDays(), this.toHours() % 24)

fun Duration.toHoursAndMinutes() =
    String.format("%sh %sm", this.toHours(), this.toMinutes() % 60)

fun Duration.toMinutesAndSeconds() =
    String.format("%sm %ss", this.toMinutes(), this.seconds % 60)

fun Duration.toSecondsAndMillis() =
    String.format("%ss %sms", this.seconds, this.toMillis() % 1000)