package extension

import net.dv8tion.jda.core.entities.Member
import java.util.*

fun Member.isAdmin(): Boolean {
    val roleNames = this.roles.map { it.name }
    val adminRoles = config.Guild.getAdminRoles(this.guild.idLong)
    return !Collections.disjoint(adminRoles, roleNames)
}
