package config

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class WarningTest {

    @Test
    fun warningsAreInitialized() {
        assertThat(Warning.naughtyTextWarning).isNotBlank()

        assertThat(Warning.naughtyEmoteWarning).isNotBlank()

        assertThat(Warning.embedSpamWarning).isNotBlank()
        val humanReadableDuration = "test"
        assertThat(Warning.embedSpamWarning.format(humanReadableDuration)).contains(humanReadableDuration)
    }
}