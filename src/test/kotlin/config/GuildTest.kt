package config

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GuildTest {

    @Test
    fun guildsAreInitialized() {
        assertThat(Guild.config).isNotEmpty
        assertThat(Guild.config).containsKey(Guild.DFAC)
        assertThat(Guild.config).containsKey(Guild.BACON_LOVERS)
        assertThat(Guild.getChannel(Guild.DFAC, Guild.Channel.WELCOME)).isEqualTo(364474910632050689)
    }
}
