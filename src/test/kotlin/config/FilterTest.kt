package config

import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReaderBuilder
import extension.containsBannedText
import extension.withReplacedCharacters
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.rules.TestName
import org.junit.Rule

class FilterTest {

    companion object {
        private const val BANNED_TEXT_FILE = "banned_text.csv"
        private const val NO_BANNED_TEXT_FILE = "no_banned_text.csv"
    }

    @get:Rule
    var testName = TestName()
    private var startTime = System.currentTimeMillis()

    @Before
    fun beforeMethod() {
        startTime = System.currentTimeMillis()
    }

    @After
    fun afterMethod() {
        println("${testName.methodName} ${System.currentTimeMillis() - startTime}")
    }

    @Test
    fun containsBannedText_isCaught() {
        for (line in readTextFromCsv(BANNED_TEXT_FILE)) {
            assertThat(line.containsBannedText()).isTrue()
        }
    }

    @Test
    fun containsNoBannedText_isNotCaught() {
        for (line in readTextFromCsv(NO_BANNED_TEXT_FILE)) {
            assertThat(line.containsBannedText()).isFalse()
        }
    }

    @Test
    fun containsReplacableCharacter_isReplaced() {
        val nameToNickname = mapOf(
            "aaaa" to "aaaа",
            "kkkk" to "kkkᴋ",
            "ffaf" to "ffаf",
            "vkvv" to "vᴋvv"
        )
        nameToNickname.forEach { (original, expected) ->
            val nickname = original.withReplacedCharacters()
            assertThat(nickname).isNotEqualTo(original)
            assertThat(nickname).isEqualTo(expected)
        }
    }

    @Test
    fun containsNoReplacableCharacter_dotIsAdded() {
        val nameToNickname = mapOf(
            "ffff" to "ffff.",
            "vvvv" to "vvvv."
        )
        nameToNickname.forEach { (original, expected) ->
            val nickname = original.withReplacedCharacters()
            assertThat(nickname).isNotEqualTo(original)
            assertThat(nickname).isEqualTo(expected)
        }
    }

    private fun readTextFromCsv(fileName: String): List<String> {
        val inputStream = javaClass.classLoader.getResourceAsStream(fileName)
        val csvParser = CSVParserBuilder().withSeparator('\t').withIgnoreQuotations(true).build()
        val csvReader = CSVReaderBuilder(inputStream.bufferedReader()).withCSVParser(csvParser).build()
        val lines = csvReader.readAll().mapNotNull { it[0] }.filter { it.isNotBlank() }
        csvReader.close()
        return lines
    }
}