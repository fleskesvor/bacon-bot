package config

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MessageTest {

    private val mentionPattern = ".*%s\\b.*"

    @Test
    fun messagesAreInitialized() {
        assertThat(Message.welcomeMessages(Guild.BACON_LOVERS)).isNotEmpty

        Message.welcomeMessages(Guild.BACON_LOVERS)!!.forEach { message ->
            assertThat(message).isNotBlank()
            assertThat(message).containsPattern(mentionPattern)
        }

        val mention = "@test#1234"
        val welcomeMessage = Message.welcomeMessage(Guild.BACON_LOVERS, mention)
        assertThat(welcomeMessage).isNotBlank()
        assertThat(welcomeMessage).contains(mention)
    }
}
