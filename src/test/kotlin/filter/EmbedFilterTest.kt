package filter

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class EmbedFilterTest {

    @Test
    fun idsAreChecked_onCooldownFromSecondCall() {
        val id1 = "test1"
        val id2 = "test2"

        assertThat(isOnCooldown(id1)).isFalse()
        assertThat(isOnCooldown(id1)).isTrue()
        assertThat(isOnCooldown(id1)).isTrue()

        assertThat(isOnCooldown(id2)).isFalse()
        assertThat(isOnCooldown(id2)).isTrue()
        assertThat(isOnCooldown(id2)).isTrue()

        // TODO: Adjust expiry while testing, and assert that cooldown runs out
    }
}