package service

import extension.getTextChannelByType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.MessageEmbed
import net.dv8tion.jda.core.entities.TextChannel
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.ArgumentCaptor

class DatabaseServiceTest {

    @Test
    fun contentIsValid_parsedToRow() {
        val content = "id123 key1:value1 key2:true key3:\"another\" value\""
        val row = parseContent(content)
        assertThat(row).isNotNull
        assertThat(row!!.key).isEqualTo("id123")
        assertThat(row.values[0].first).isEqualTo("key1")
        assertThat(row.values[0].second).isEqualTo("value1")
        assertThat(row.values[1].first).isEqualTo("key2")
        assertThat(row.values[1].second).isEqualTo("true")
        assertThat(row.values[2].first).isEqualTo("key3")
        assertThat(row.values[2].second).isEqualTo("another\" value")
    }

    @Test
    fun contentIsInvalid_notParsedToRow() {
        val content = "123 key1 key2: false"
        val row = parseContent(content)
        assertThat(row).isNull()
    }

    @Test
    fun saveContent_embedContainsAllValues() {
        val row = Row("id123", arrayListOf("key1" to "value1" , "key2" to "true", "key3" to "\"another\" value\""))
        val channel = mockChannel()

        service.saveContent(row, channel.guild)

        val captor = ArgumentCaptor.forClass(MessageEmbed::class.java)
        verify(channel).sendMessage(captor.capture())
        val embed = captor.value

        assertThat(embed).isNotNull
        assertThat(embed.description).isEqualTo(row.key)
        assertThat(embed.fields).isNotEmpty
        assertThat(embed.fields[0].name).isEqualTo(row.values[0].first)
        assertThat(embed.fields[0].value).isEqualTo(row.values[0].second)
        assertThat(embed.fields[1].name).isEqualTo(row.values[1].first)
        assertThat(embed.fields[1].value).isEqualTo(row.values[1].second)
        assertThat(embed.fields[2].name).isEqualTo(row.values[2].first)
        assertThat(embed.fields[2].value).isEqualTo(row.values[2].second)
    }

    private fun mockChannel(): TextChannel {
        val jda = mock(JDA::class.java)
        val guild = mock(Guild::class.java)
        val channel = mock(TextChannel::class.java)

        `when`(guild.idLong).thenReturn(config.Guild.BACON_LOVERS)
        `when`(guild.jda).thenReturn(jda)
        `when`(guild.getTextChannelByType(config.Guild.Channel.LOGS)).thenReturn(channel)

        `when`(channel.guild).thenReturn(guild)

        return channel
    }
}
