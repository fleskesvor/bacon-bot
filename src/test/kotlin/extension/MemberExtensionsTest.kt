package extension

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class MemberExtensionsTest {

    private val validAdminRoleName = "Helper People"
    private val inValidAdminRoleName = "Pelper Heople"

    private val guild = mock(Guild::class.java)

    @Before
    fun setUp() {
        `when`(guild.idLong).thenReturn(560575039519326229L)
    }

    @Test
    fun hasNoRole_isNotAdmin() {
        val member = mockMember()
        val isAdmin = member.isAdmin()
        assertThat(isAdmin).isFalse()
    }

    @Test
    fun hasNonAdminRole_isNotAdmin() {
        val member = mockMember(inValidAdminRoleName)
        val isAdmin = member.isAdmin()
        assertThat(isAdmin).isFalse()
    }

    @Test
    fun hasAdminRole_isAdmin() {
        val member = mockMember(validAdminRoleName)
        val isAdmin = member.isAdmin()
        assertThat(isAdmin).isTrue()
    }

    private fun mockMember(roleName: String? = null): Member {
        val member = mock(Member::class.java)

        val role = mock(Role::class.java)
        `when`(role.name).thenReturn(roleName)
        `when`(member.roles).thenReturn(listOf(role))

        `when`(member.guild).thenReturn(guild)
        return member
    }
}
