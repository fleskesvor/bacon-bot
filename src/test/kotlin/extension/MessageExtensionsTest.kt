package extension

import config.Command
import net.dv8tion.jda.core.entities.Message
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class MessageExtensionsTest {

    private val message = mock(Message::class.java)

    @Test
    fun regularMessage_containsNoCommand() {
        `when`(message.contentRaw).thenReturn("test")

        val (command, content) = message.parse()
        assertThat(command).isNull()
        assertThat(content).isEqualTo("test")
    }

    @Test
    fun pingMessage_containsCommand() {
        `when`(message.contentRaw).thenReturn("!ping")

        val (command, content) = message.parse()
        assertThat(command).isEqualTo(Command.PING)
        assertThat(content).isBlank()
    }

    @Test
    fun whoisMessage_containsCommandAndContent() {
        `when`(message.contentRaw).thenReturn("!whois 12345")

        val (command, content) = message.parse()
        assertThat(command).isEqualTo(Command.WHOIS)
        assertThat(content).isEqualTo("12345")
    }
}
