package extension

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.Duration

class TimeExtensionsTest {

    @Test
    fun durationInDays_hasDayAndHourFormat() {
        val durationString = Duration.ofDays(13).plusHours(17).toHumanReadable()
        assertThat(durationString).isEqualTo("13d 17h")
    }

    @Test
    fun durationInHours_hasHourAndMinuteFormat() {
        val durationString = Duration.ofHours(13).plusMinutes(17).toHumanReadable()
        assertThat(durationString).isEqualTo("13h 17m")
    }

    @Test
    fun durationInMinutes_hasMinuteAndSecondFormat() {
        val durationString = Duration.ofMinutes(13).plusSeconds(17).toHumanReadable()
        assertThat(durationString).isEqualTo("13m 17s")
    }

    @Test
    fun durationInSeconds_hasMinuteAndMillisFormat() {
        val durationString = Duration.ofSeconds(13).plusMillis(17).toHumanReadable()
        assertThat(durationString).isEqualTo("13s 17ms")
    }

}